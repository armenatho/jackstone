﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnFish : MonoBehaviour {

    public GameObject[] fishes;
    public float waitTime;
    public GameObject leftBoundary;
    public GameObject rightBoundary;
    public GameObject upperBoundary;
    public GameObject lowerBoundary;
    private float leftBound;
    private float rightBound;
    private float upperBound;
    private float lowerBound;

	// Use this for initialization
	void Start () {
        leftBound = leftBoundary.transform.position.x;
        rightBound = rightBoundary.transform.position.x;
        upperBound = upperBoundary.transform.position.y;
        lowerBound = lowerBoundary.transform.position.y;
        StartCoroutine("Spawn");

	}
	
	// Update is called once per frame
	void FixedUpdate () {

	}

    IEnumerator Spawn(){
        while (true){
            float odds = Random.Range(0f, 100f);
            bool rightward = odds < 50;
            float x;
            if (rightward){
                x = leftBound;
            } else {
                x = rightBound;
            }
            float y = Random.Range(lowerBound, upperBound);
         
            MoveFish newFish = Instantiate(fishes[0], new Vector3(x, y), Quaternion.identity).GetComponent<MoveFish>();
            newFish.speed = Random.Range(1f, 2f);

            if (rightward)
            {
                newFish.direction = 1;
                newFish.xLimit = rightBound;
            }
            else
            {
                newFish.direction = -1;
                newFish.xLimit = leftBound;
            }

            yield return new WaitForSeconds(waitTime);
        }
    }
}
