﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveFish : MonoBehaviour {

    public int direction;
    public float speed;
    public float xLimit;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.right * speed * Time.deltaTime * direction);
        if(direction == 1)
        {
            if(transform.position.x > xLimit)
            {
                Destroy(gameObject);
            }
        }
        if(direction == -1)
        {
            if(transform.position.x < xLimit)
            {
                Destroy(gameObject);
            }
        }
	}
}
